﻿const http = require('http');
const https = require('https');
const PORT = process.env.PORT || 3000;

function handler(req, res) {

	let word = req.url.replace('/', '');
	res.writeHead(200, 'OK', {'Content-Type': 'text/plane; charset=utf-8'});
	if (!word) {
		res.write('Введите в строке адреса слово для перевода');
		return res.end();
	}

	url = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
		+ '?key=trnsl.1.1.20160723T183155Z.f2a3339517e26a3c.d86d2dc91f2e374351379bb3fe371985273278df'
		+ '&lang=ru-en'
		+ '&text=' + word;

	const request = https.request(url, (response) => {
		let data = '';
		response.on('data', function (chunk) {
			data += chunk;
		});
		response.on('end', function () {
			let result = JSON.parse(data);
			res.write(`Перевод: ${result.text}`);
			res.end();
		});
	});

	request.end();
}

const server = http.createServer();
server.on('error', err => console.error(err));
server.on('request', handler);
server.on('listening', () => {
	console.log('Start HTTP on port %d', PORT);
});

server.listen(PORT);